@extends('templates.principal')

@section('titulo', 'Carros')

@section('conteudo')
    <button class="btn btn-success" onclick="window.location.href='{{ Route('carro.create') }}'">Adicionar Carro</button>
    <hr>

    <h4>Lista de carros</h4>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Modelo</th>
                <th>Versão</th>
                <th>Descrição</th>
                <th>Preço</th>
                <th>Ações</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($carros as $carro)
                <tr>
                    <td>{{$carro['modelo']}}</td>
                    <td>{{$carro['versao']}}</td>
                    <td>{{$carro['descricao']}}</td>
                    <td>{{$carro['preco']}}</td>
                    <td>
                        <button class="btn btn-warning" onclick="window.location.href='/edit/{{$carro['id']}}'">Editar</button>
                        <button class="btn btn-danger">Excluir</button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection