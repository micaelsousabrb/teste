@extends('templates.principal')

@section('titulo', 'Cadastrar Carro')

@section('conteudo')
    <h4>Editar {{$carro['modelo']}} {{ $carro['versao']}}</h4> <hr>

    <div style="height: 33rem; width: 100%; display: flex; align-items: center;">
        <div class="col-md-12">
            <form method="POST" action="/update/{{$carro['id']}}">
                @csrf
                @method('PUT')
                <div class="form-group row">
                    <label for="modelo" class="col-md-2 col-form-label">Modelo <span style="color: #d63031;">*</span></label>
                    <div class="col-md-10">
                        @error('modelo')
                            <small style="color: #d63031;">{{$message}}</small>
                        @enderror
                        <input class="form-control @error('modelo') is-invalid @enderror" type="text" name="modelo" id="modelo" placeholder="Digite o modelo do carro" value="{{$carro['modelo']}}" value="{{old('modelo')}}">
                    </div>
                </div>
        
                <div class="form-group row">
                    <label for="versao" class="col-md-2 col-form-label">Versão <span style="color: #d63031;">*</span></label>
                    <div class="col-md-10">
                        @error('versao')
                            <small style="color: #d63031;">{{$message}}</small>
                        @enderror
                        <input class="form-control @error('versao') is-invalid @enderror" type="text" name="versao" id="versao" placeholder="Digite a versão do carro" value="{{$carro['versao']}}" value="{{old('versao')}}">
                    </div>
                </div>
        
                <div class="form-group row">
                    <label for="descricao" class="col-md-2 col-form-label">Descrição <span style="color: #d63031;">*</span></label>
                    <div class="col-md-10">
                        @error('descricao')
                            <small style="color: #d63031;">{{$message}}</small>
                        @enderror
                        <input class="form-control @error('descricao') is-invalid @enderror" type="text" name="descricao" id="descricao" rows="5" placeholder="Digite a descrição do carro" value="{{$carro['descricao']}}" value="{{old('descricao')}}"/>
                    </div>
                </div>
        
                <div class="form-group row">
                    <label for="preco" class="col-md-2 col-form-label">Preço <span style="color: #d63031;">*</span></label>
                    <div class="col-md-10">
                        @error('preco')
                            <small style="color: #d63031;">{{$message}}</small>
                        @enderror
                        <input class="form-control @error('preco') is-invalid @enderror" type="text" name="preco" id="preco" placeholder="Digite o preço do carro" value="{{$carro['preco']}}" value="{{old('preco')}}">
                    </div>
                </div>

                <div style="display: flex; justify-content: flex-end">
                    <button type="button" class="btn btn-secondary" style="margin-right: 0.5rem;" onclick="window.location.href='{{ Route('carro.index') }}'">Voltar</button>
                    <button type="submit" class="btn btn-success">Editar</button>
                </div>
            </form>
        </div>
    </div>
@endsection