<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    <title>Error</title>
</head>
<body>
    <div style="height: 98vh; display: flex; align-items: center; justify-content: center; font-family: 'Roboto', sans-serif;">
        <h1>Opsss! {{$error}}</h1>
    </div>
</body>
</html>