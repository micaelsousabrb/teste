<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CarroController;

Route::get('/', [CarroController::class, 'index'])->name('carro.index');
Route::get('/create', [CarroController::class, 'create'])->name('carro.create');
Route::post('/store', [CarroController::class, 'store'])->name('carro.store');
Route::get('/edit/{id}', [CarroController::class, 'edit'])->name('carro.edit');
Route::put('/update/{id}', [CarroController::class, 'update'])->name('carro.edit');