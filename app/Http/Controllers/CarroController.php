<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\CarroRepository;
use App\Http\Requests\Carro;

class CarroController extends Controller
{
    private $carroRepository;

    public function __construct(CarroRepository $carroRepository)
    {
        $this->carroRepository = $carroRepository;
    }

    public function index()
    {
        $carros = $this->carroRepository->listarTodosOsCarros();

        $dadosIndex = [
            'carros' => $carros
        ];

        return view('carro.index', $dadosIndex);
    }

    public function create()
    {
        return view('carro.create');
    }

    public function store(Carro $request)
    {
        $this->carroRepository->cadastrarCarro($request);

        return redirect(Route('carro.index'));
    }

    public function edit($id)
    {
        try
        {
            $carro = $this->carroRepository->buscarCarroPeloId($id);

            $dadosEdit = [
                'carro' => $carro
            ];
    
            return view('carro.edit', $dadosEdit);
        }
        catch (\Exception $e) 
        {
            return view('customizado.error', ['error' => $e->getMessage()]);
        }
    }

    public function update(Carro $request, $id)
    {
        try 
        {
            $this->carroRepository->editarCarro($request, $id);

            return redirect(Route('carro.index'));
        }
        catch (\Exception $e)
        {
            return view('customizado.error', ['error' => $e->getMessage()]);
        }
    }
}
