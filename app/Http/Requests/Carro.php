<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Carro extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'modelo' => 'required|min:2|max:30',
            'versao' => 'required|min:2|max:20',
            'descricao' => 'required|min:2|max:200',
            'preco' => 'required|numeric'
        ];
    }
}
