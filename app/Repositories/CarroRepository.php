<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\Carro;

class CarroRepository
{
    public function listarTodosOsCarros()
    {
        return Carro::all();
    }

    public function cadastrarCarro($request)
    {
        Carro::create($request->all());
    }

    public function buscarCarroPeloId($id)
    {
        $carro = Carro::find($id);

        if (empty($carro))
        {
            throw new \Exception('Carro não encontrado!');
        }

        return Carro::find($id);
    }

    public function editarCarro($request, $id)
    {
        $carro = $this->buscarCarroPeloId($id);

        $carro->update($request->all());

        if (!$carro->save())
        {
            throw new \Exception('Ocorreu um erro ao editar o carro!');
        }
    }
}